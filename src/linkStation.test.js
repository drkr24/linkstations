import { expect } from 'chai';
import calculate, { assignStation, printFormatted } from './linkStation';

describe('Link stations', () => {

  // main link station task
  it('should calculate stations for given points', () => {
    const points = [
      { x: 0, y: 0},
      { x: 100, y: 100},
      { x: 15, y: 10},
      { x: 18, y: 18}
    ];
    
    const stations = [
      [0, 0, 10],
      [20, 20, 5],
      [10, 0, 12]
    ];
    
    calculate(stations, points)
    
  });

  //arbitrary tests
  it('should assign station to a point', () => {
  
    const point = { 'x': 1, 'y': 1 };
    const station = [ 1, 2, 2 ]; //x, y, reach
    const expectedStation = { 'stationX' : station[0], 'stationY': station[1], 'power': 1 };
    
    const resultPoint = assignStation(point, station);

    expect(resultPoint.bestStation).to.deep.equal(expectedStation);
  });
  
  it('should not assign station to a point out of reach', () => {
  
    const point = { 'x': 0, 'y': 0 };
    const station = [ 0, 2, 1 ]; //x, y, reach
    
    const resultPoint = assignStation(point, station);
    expect(resultPoint).to.not.have.property('bestStation');
  });

  it('should format correctly output message', () => {
  
    const givenPointWithoutStation = { 'x': 1, 'y': 2 };

    const bestStation = { 'stationX' : 3, 'stationY': 4, 'power': 5 };
    const givenPointWithStation = { ...givenPointWithoutStation, bestStation };

    expect(printFormatted(givenPointWithoutStation))
      .to.equal("No link station within reach for point 1,2");

    expect(printFormatted(givenPointWithStation))
      .to.equal("Best link station for point 1,2 is 3,4 with power 5");
  });

});