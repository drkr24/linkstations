
export default function calculate(stations, points) {

    points.map(point => stations.forEach(station => assignStation(point, station)));
    points.forEach(point => console.log(printFormatted(point)));
};

export function assignStation(point, station) {

    const [stationX, stationY, reach] = station;
    const dist = Math.sqrt(Math.pow(stationX - point.x, 2) + Math.pow(stationY - point.y, 2));
    const power = dist > reach ? 0 : Math.pow(reach - dist, 2);

    if (power == 0) {
        return point;
    }

    if (!point.bestStation || point.bestStation.power < power) {
        point.bestStation = { 'stationX' : stationX, 'stationY': stationY, 'power': power };
    }
    return point;
};

export function printFormatted(point) {

    if (!point.bestStation) {
        return `No link station within reach for point ${point.x},${point.y}`
    }

    const {stationX, stationY, power} = point.bestStation;
    return `Best link station for point ${point.x},${point.y} is ${stationX},${stationY} with power ${power}`;
};